
<?php




class Person
{

    public $name;
    public $lastname;
    public $birthdate;



    public function __construct($name, $lastname, $birthdate)
    {
        $this->name = $name;
        $this->lastname = $lastname;
        $this->birthdate = $birthdate;
    }
}

class Database
{

    public $people;

    public function add_person($person)
    {
        $this->people[] = $person;
    }

    public function search($operator, $n)
    {
        if ($operator == "do" || $operator == "od" || $operator == "rowne") {

            echo "Lista osób w wieku $operator $n lat: <br />";

            foreach ($this->people as $key => $person) {

                $dob = new DateTime($person->birthdate);
                $now = new DateTime();
                $difference = $now->diff($dob);
                $age = $difference->y;

                $form = "Imie $person->name <br />
                Nazwisko $person->lastname <br />
                Data urodzin $person->birthdate r.<br />" .
                    'Wiek to ' . $age . " lat <br />";
                


                if ($operator == "do" && $age < $n) {
                    echo $form;
                    continue;
                }
                
                if ($operator == "od" && $age > $n) {
                    echo $form;
                    continue;
                } 
                
                if ($operator == "rowne" && $age == $n) {
                    echo $form;
                    continue;
                }
            }
        } else {
            echo "Możliwe warianty wyszukiwania to _od_ , _do_ i _rowne_ ";
        }
    }
}
